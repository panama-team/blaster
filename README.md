# Blaster

This repository contains all the code to reproduce the results of the paper *BLASTER: an off-grid Method for Blind and Regularized Acoustic Echoes Retrieval*

## Abstract

Acoustic echoes retrieval is a research topic that is gaining importance in many speech and audio signal processing applications (speech enhancement,source separation, dereverberation, room geometry estimation, etc.). This work proposes a novel approach to retrieve acoustic echoes timing off-grid and blindly, i.e., from a multichannel recording of an unknown sound source such as speech.

It builds on the recent framework of continuous dictionaries. In contrast with existing methods, the proposed approach does not rely on parameter tuning nor peak picking techniques by working directly in the parameter space of interest. The accuracy and robustness of the method are assessed on challenging simulated setups with varying noise and reverberation levels and are compared to two state-of-the-art methods

### Keywords

*Blind Channel Identification*, *Super Resolution*, *Sparsity*, *Acoustic Impulse Response*

## Authors

Diego Di Carlo, Clement Elvira, Antoine Deleforge, Nancy Bertin, Remi Gribonval

---

### Contact

[Diego Di Carlo](diego.di-carlo@inria.fr) or [Clement Elvira](clement.elvira@inria.fr)

#### BibTeX

```latex
@INPROCEEDINGS{DiCarlo2020, 
	author={{Carlo}, D. D. and {Elvira}, C. and {Deleforge}, A. and {Bertin}, N. and {Gribonval}, R.}, 
	booktitle={ICASSP 2020 - 2020 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)}, 
	title={Blaster: An Off-Grid Method for Blind and Regularized Acoustic Echoes Retrieval},
	year={2020}, 
	volume={}, 
	number={}, 
	pages={156-160},
	doi={10.1109/ICASSP40776.2020.9054647},
	} 
```


## Repository Structure

```
-> blaster/     # python package for off-grid channel estimantion
    |--> make_data/         # routines for data generation
    |--> visualization/     # routines for plotting
-> examples/    # some easy-to-use script
-> figures/     # figures used in the paper
-> icassp2020_experiments/  # script for running the experiments
-> icassp2020_evaluation/   # script for evaluation
-> complementary_materials/ # source TeX files for complementary material pdf
```

## Data

Please download the data used for the evaluation at this [link]() (hdf5 files). Otherwise your can generate them with the script in `blaster/make_data.py`

## Selected Results from the paper

### Channels and Plots

### Evaluations

## Running the Code

### Dependencies

## Recreate the Figure of the Papers

## License

This software is distributed under the [MIT license](license).