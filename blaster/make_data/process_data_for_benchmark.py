import glob
import h5py
import subprocess
import socket
import numpy as np
import pandas as pd
import soundfile as sf
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from natsort import natsorted

from tqdm import tqdm

from src.channel import ImageSource_Channel, Channel, anchor_hp
from src.source import Source
from src.file_utils import save_to_pickle, make_dirs, load_from_pickle
from src.dsp_utils import awgn, make_same_size

uniform = lambda dim, a, b : (b-a) * np.random.random(dim) + a

datasets = ['benchmark_test_snr', 
          'benchmark_test_rt60',
          'benchmark_test_valid'
]

dataset = datasets[0]
data_from_igrida = 'icassp2020_dataset_772d4_igrida'
data_from_igrida = 'icassp2020_dataset_af1051_igrida'
path_to_dataset = './data/processed/%s/%s/' % (data_from_igrida, dataset)

# hyperparameters
Fs = 16000
do_anchor_hp = True

# dataset-related parameters
datasets_params = {
    'benchmark_test_snr': {
        'seed': 666,
        # room
        'rt60': [0.4],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [200, 20, 14, 6, 0],
        'tau_min_sep': 0,
    },
    'benchmark_test_rt60': {
        'seed': 667,
        # room
        'rt60': [0.2, 0.4, 0.6, 0.8, 1.],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [20],
        'tau_min_sep': 0,
    },
    'benchmark_test_valid': {
        'seed': 668,
        # room
        'rt60': [0.2, 1.],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [20],
        'tau_min_sep': 0,
    }
}


def pickle_dataset_to_hdf5_dataset(path_to_dataset, dataset, params):
    path_to_pkl_dataset = path_to_dataset + 'data/'
    path_to_output = path_to_dataset
    print('Saving dataset in', path_to_output)

    # set the seed for reproducibility
    np.random.seed(params['seed'])

    # some paths
    path_to_output_data = path_to_output + dataset + '_data.hdf5'
    path_to_output_info = path_to_output + dataset + '_info.csv'

    #######################################
    # Initialize the dataset and structures
    f = h5py.File(path_to_output_data, 'w')
    # open it in append mode
    f = h5py.File(path_to_output_data, 'a')
    # add dataset parameters as hdf5 dataset attributes
    f.attrs['name'] = dataset
    f.attrs['Fs'] = Fs
    f.attrs['do_anchor_hp'] = 1 if do_anchor_hp else 0
    

    for key, val in params.items():
        f.attrs[key] = str(val)

    f.create_group(dataset)
    f.create_group(dataset + '/obs')
    f.create_group(dataset + '/flt')
    f.create_group(dataset + '/src')

    # # Initialize the info
    df = pd.DataFrame()

    # get file from folder
    files = natsorted(glob.glob(path_to_pkl_dataset + '*.pkl'))

    for c, file in enumerate(tqdm(files)):

        # load pickle
        sample_dict = load_from_pickle(file)
        if sample_dict['signal'] == 'speech' and dataset == "benchmark_test_valid":
            continue

        obs = sample_dict['obs']
        flt = sample_dict['flt']
        src = sample_dict['src']
        df.at[c, 'src_mic_id'] = sample_dict['src_mic_id']
        df.at[c, 'src_timit_id'] = sample_dict['src_timit_id']
        df.at[c, 'abs'] = sample_dict['abs']
        df.at[c, 'signal'] = sample_dict['signal']

        df.at[c, 'snr'] = sample_dict['snr']
        df.at[c, 'rt60'] = sample_dict['rt60']

        # time of the early reflections
        ch1_coeff = np.argsort(flt[:, 1, 0])[::-1][:10]
        ch2_coeff = np.argsort(flt[:, 1, 1])[::-1][:10]

        t_max = max(np.max(flt[ch1_coeff, 0, 0]),
                    np.max(flt[ch1_coeff, 0, 1]))

        # sf.write('./%d_src.wav' % i, src, Fs)
        # sf.write('./%d_m1.wav' % i, obs[:, 0], Fs)
        # sf.write('./%d_m2.wav' % i, obs[:, 1], Fs)

        # save filter, source and observation
        f.create_dataset(dataset + "/obs/" + str(c), data=obs)
        f.create_dataset(dataset + "/flt/" + str(c), data=flt)
        f.create_dataset(dataset + "/src/" + str(c), data=src)

        df.to_csv(path_to_output_info, sep=',')

if __name__ == "__main__":
    pickle_dataset_to_hdf5_dataset(
        path_to_dataset, dataset, datasets_params[dataset])
    pass
