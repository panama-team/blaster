import argparse
import h5py
import subprocess
import socket
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from tqdm import tqdm

from src.channel import ImageSource_Channel, Channel, anchor_hp
from src.source import Source
from src.file_utils import save_to_pickle, make_dirs
from src.dsp_utils import awgn, make_same_size

uniform = lambda dim, a, b : (b-a) * np.random.random(dim) + a

datasets = ['benchmark_test_snr', 
            'benchmark_test_rt60',
            'benchmark_test_valid'
]

# hyperparameters
Fs = 16000
t_max = 10000
do_anchor_hp = True

############################
# GENERATE BENCHMARK DATASET

# set up the output folder
# exp_name = '_'.join(datasets) + '_dataset'
exp_name = 'icassp2020_dataset'
# get commit id
git_commit_id = subprocess.check_output(
    ["git", "describe", "--always"]).strip()
git_commit_id = str(git_commit_id).replace('b', '').replace("'", '')
# get hostname
hostname = socket.gethostname()
hostname = 'igrida' if 'igrida' in hostname else hostname
hostname = 'embuscade' if 'embuscade' in hostname else hostname

main_directory = '%s_%s_%s' % (exp_name, git_commit_id, hostname)

path_to_output = './data/processed/%s/' % main_directory 
make_dirs(path_to_output)

print('Saving dataset in', path_to_output)

# dataset-related parameters
datasets_params = {
    'benchmark_test_snr': {
        'seed': 666,
        # room
        'rt60': [0.4],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [200, 20, 14, 6, 0],
        'tau_min_sep': 0,
    },
    'benchmark_test_snr_200': {
        'seed': 200,
        # room
        'rt60': [0.2],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [200, 20, 14, 6, 0],
        'tau_min_sep': 0,
    },
    'benchmark_test_rt60': {
        'seed': 667,
        # room
        'rt60': [0.2, 0.4, 0.6, 0.8, 1.],  # ms
        # source
        'source_signal': ['noise', 'speech'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [20],
        'tau_min_sep': 0,
    },
    'benchmark_test_valid': {
        'seed': 668,
        # room
        'rt60': [0.2, 1.],  # ms
        # source
        'source_signal': ['noise'],  # noise, speech
        # filter
        'n_dirac': 3000,
        'snr': [0, 20],
        'tau_min_sep': 0,
    }
}

# internal exception
class TooCloseTauException(Exception):
    pass


def room_sizes_from_rt60(n_sample,rt60_arr):
    assert rt60_arr.shape[-1] == n_sample
    x = uniform([1, n_sample], 3, 8)
    y = uniform([1, n_sample], 3, 8)
    z = uniform([1, n_sample], 2, 6)
    volumes = x*y*z
    assert volumes.shape == (1, n_sample)
    room_sizes = np.vstack([x, y, z])
    assert room_sizes.shape == (3, n_sample)
    total_surfaces = 2*(x*y + x*z + y*z)
    assert total_surfaces.shape == (1, n_sample)
    alphas = 0.161*volumes/(total_surfaces * rt60_arr)
    alphas[alphas > 1] = 1

    # show distribution of alphas
    # n, bins, patches = plt.hist(alphas.squeeze(), 200, facecolor='blue', alpha=0.5)
    # plt.show()

    return room_sizes, volumes, alphas


def get_mic_source_position(room_sizes, array=None):
    n_sample = room_sizes.shape[-1]

    src_pos  = uniform([3, n_sample], 0.20, 0.80) * room_sizes

    if array is None:
        mic1_pos = uniform([3, n_sample], 0.25, 0.75) * room_sizes
        mic2_pos = uniform([3, n_sample], 0.25, 0.75) * room_sizes
    else:
        raise NotImplementedError()

    assert src_pos.shape == mic1_pos.shape == mic2_pos.shape == (3, n_sample)

    return src_pos, mic1_pos, mic2_pos


def plot_and_save_src_mic(src_pos, mic1_pos, mic2_pos, save_as):
    n_sample = src_pos.shape[-1]
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    ax.scatter(src_pos[0, :],
            src_pos[1, :],
            src_pos[2, :],
            label='sources')

    ax.scatter(mic1_pos[0, :],
            mic1_pos[1, :],
            mic1_pos[2, :],
            label='mic1')

    ax.scatter(mic2_pos[0, :],
            mic2_pos[1, :],
            mic2_pos[2, :],
            label='mic2')

    for i in range(n_sample):
        ax.plot([mic1_pos[0, i], mic2_pos[0, i]],
                [mic1_pos[1, i], mic2_pos[1, i]],
                [mic1_pos[2, i], mic2_pos[2, i]], alpha=0.1)

    for i in range(n_sample):
        ax.plot([(mic1_pos[0, i] + mic2_pos[0, i])/2, src_pos[0, i]],
                [(mic1_pos[1, i] + mic2_pos[1, i])/2, src_pos[1, i]],
                [(mic1_pos[2, i] + mic2_pos[2, i])/2, src_pos[2, i]], alpha=0.1)


    plt.legend()
    plt.title('Room configuation for dataset ' + dataset)
    plt.savefig(save_as + 'room_configuration.pdf')
    plt.close()

    np.savetxt(save_as + 'src_pos.csv', src_pos)
    np.savetxt(save_as + 'mic1_pos.csv', mic1_pos)
    np.savetxt(save_as + 'mic2_pos.csv', mic2_pos)
    return


def get_tau_amp(d, src_pos, mic1_pos, mic2_pos, 
                room_abs, room_size,
                check_tau_min_dist=False, thr=1):
    mics_pos = [mic1_pos, mic2_pos]
    im_channel = ImageSource_Channel(
        source_position=src_pos,
        mics_position=mics_pos,
        absorption=room_abs, room_size=room_size.squeeze(),
        fs=Fs, filter_length=t_max, max_n_diracs=d)
    rir1_toa_coeff_dict = im_channel.get_loc_and_coeff_dict(mic=0)
    rir2_toa_coeff_dict = im_channel.get_loc_and_coeff_dict(mic=1)

    rir1 = Channel(
        coeff=rir1_toa_coeff_dict['coeff'],
        toa=rir1_toa_coeff_dict['toa'],
        filter_length=t_max,
        )


    rir2 = Channel(
        coeff=rir2_toa_coeff_dict['coeff'],
        toa=rir2_toa_coeff_dict['toa'],
        filter_length=t_max,
        )

    if check_tau_min_dist:
        if rir1.are_too_close_tau(thr=thr):
            raise TooCloseTauException('Close tau for h2')
        if rir2.are_too_close_tau(thr=thr):
            raise TooCloseTauException('Close tau for h2')


    if do_anchor_hp:
        rir1, rir2 = anchor_hp(rir1, rir2)

    ####################
    # Prepare for saving
    ch1np = np.array([rir1.toa, rir1.coeff[:-1]]).T
    ch2np = np.array([rir2.toa, rir2.coeff[:-1]]).T

    return rir1, rir2, ch1np, ch2np


def run_generation(path_to_output, dataset, n_sample, params, sgn_length, s):
    
    # set the seed for reproducibility
    np.random.seed(params['seed'])

    # some paths
    make_dirs(path_to_output)
    path_to_output_data = path_to_output + 'data/'
    make_dirs(path_to_output_data)

    # current params
    n_dirac = params['n_dirac']
    tau_min_sep = params['tau_min_sep']
    rt60 = params['rt60']
    snr = params['snr']

    # room size from rt60
    if len(rt60) > 2:
        rt60_choice = np.random.choice(rt60, n_sample)
    elif len(rt60) == 2:
        rt60_choice = uniform(n_sample, rt60[0], rt60[1])
    elif len(rt60) == 1:
        rt60_choice = np.ones(n_sample) * rt60[0]
    else:
        raise ValueError()

    room_sizes, room_vols, room_abss \
        = room_sizes_from_rt60(n_sample, rt60_choice)

    # src and mics position random from room size
    src_pos, mic1_pos, mic2_pos = get_mic_source_position(room_sizes)

    # little check
    assert  src_pos.shape == (3, n_sample)
    assert mic1_pos.shape == (3, n_sample)
    assert mic2_pos.shape == (3, n_sample)

    # generete the snr
    if len(rt60) > 2:
        snr_choice = np.random.choice(snr, n_sample)
    elif len(rt60) == 2:
        snr_choice = uniform(n_sample, snr[0], snr[1])
    elif len(rt60) == 1:
        snr_choice = np.ones(n_sample) * snr[0]
    else:
        raise ValueError()
    
    plot_and_save_src_mic(src_pos, mic1_pos, mic2_pos, save_as=path_to_output)

    ####################
    # create the dataset
    curr_abs = room_abss[0, s]
    curr_rt60 = rt60_choice[s]
    curr_snr = snr_choice[s]

    # step 3. gerate the observation files
    # step 3.1 generate the source signal        
    rir1, rir2, ch1np, ch2np \
        = get_tau_amp(
            n_dirac,
            src_pos[:, s], mic1_pos[:, s], mic2_pos[:, s],
            room_abs=curr_abs,
            room_size=room_sizes[:,s],
            check_tau_min_dist=False, thr=tau_min_sep)

    assert len(rir1.toa) == n_dirac
    assert len(rir2.toa) == n_dirac

    for sgn in params['source_signal']:

        print('- Processing', s, 'signal', sgn )

        if sgn == 'noise':
            src_sgn_id = 0
        else:
            src_sgn_id = np.random.randint(1,130)

        src = Source(sgn, src_sgn_id, sgn_length, Fs)

        # step 3.2 convolve with the filter (hyper resultions)
        m1 = src.convolve(ch=rir1, fs=1024.e3)
        m2 = src.convolve(ch=rir2, fs=1024.e3)

        m1, m2 = make_same_size(m1, m2)

        # step 3.3 add noise
        m1 = awgn(m1, curr_snr)
        m2 = awgn(m2, curr_snr)

        obs = np.dstack([m1, m2]).squeeze()
        assert obs.shape[-1] == 2

        flt = np.zeros([n_dirac, 2, 2])
        flt[:, :, 0] = np.array(ch1np)
        flt[:, :, 1] = np.array(ch2np)
    
        data = {
            'obs' : obs,
            'flt' : flt,
            'src' : src.values,
            'src_mic_id' : s,
            'src_timit_id' : src_sgn_id,
            'abs' : curr_abs,
            'signal' : sgn,
            'snr' : curr_snr,
            'rt60' : curr_rt60,
        }
        save_to_pickle(data, path_to_output_data + 'id_%d_%s_obs_flt_src.pkl' % (s, sgn))

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description='Generate dataset for ICASSP2020')
    parser.add_argument(
        '-d', '--dataset', help='Test case for which generate the data', required=True, type=str)
    parser.add_argument(
        '-n', '--n_sample', help='Number of sample to generate', required=True, type=int)
    parser.add_argument(
        '-l', '--length', help='Duration of the source signal', required=True, type=float)
    parser.add_argument(
        '-s', '--sample', help='Current sample to compute', required=True, type=int)
    
    args = vars(parser.parse_args())

    dataset = args['dataset']
    sgn_duration = args['length']
    n_sample = args['n_sample']
    s = args['sample']

    path_to_output = path_to_output + '/' + dataset + '/'
    run_generation(path_to_output, dataset, n_sample,
                   datasets_params[dataset], sgn_duration, s)
    pass
