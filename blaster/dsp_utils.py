import numpy as np
import scipy.signal as sg
from joblib import Parallel, delayed

import librosa as lr


def awgn(x,snr_db):

    if snr_db > 100:
        print('SNR more the 100 dB, considered as noiseless')
        return x

    L = max(x.shape)
    snr_lin = 10**(snr_db/10)   # SNR to linear scale
    Ex = np.sum(np.abs(x)**2)/L # Energy of the signal
    N = Ex/snr_lin              # find the noise variance
    n = np.sqrt(N)              # standard deviation for AWGN noise
    return x + np.random.normal(0, n, L)


def make_same_size(x, y):
    L1 = x.size
    L2 = y.size
    L = max(L1, L2)
    x1 = np.zeros(L)
    y1 = np.zeros(L)
    x1[:L1] = x
    y1[:L2] = y
    return x1, y1

def DFT_matrix(N, Te=None):
    """
        Inputs:
        - N, int
        the number of samples
        - Te, int
        the sampling time
        Default 1. / N
    """

    if Te is None:
        Te = 1. / float(N)

    i, j = np.meshgrid(np.arange(N), np.arange(N))
    omega = np.exp( - 2j * np.pi * Te)
    W = np.power( omega, i * j ) / np.sqrt(N)
    return W

def apply_DFT(x, Te=None):
    """
        Inputs:
        - x, numpy array
        signal of which the DFT is computed
        - Te, int
        the sampling time
        Default 1. / N
    """

    print('Computing DFT @ x')

    assert len(x.shape) == 1
    N = x.shape[0]

    if Te is None:
        Te = 1. / float(N)

    Y = np.zeros_like(x, dtype=np.complex128)
    J = np.arange(N)
    omega = np.exp(- 2j * np.pi * Te)

    def _online_dft(i, x, omega):
        W = np.power(omega, i * J) / np.sqrt(N)
        y = W @ x
        return y

    #TODO: come up with a better computation
    # now in O(n^2)

    y_list = Parallel(n_jobs=-1)(
        delayed(_online_dft)(i, x, omega) for i in range(N))
    for i in range(len(y_list)):
        Y[i] = y_list[i]

    print('done')
    return Y


def pick_echoes(h, K):
    # Input:
    # h (L*M) : some filters
    # Output:
    # tau (K*M) : the echo locations (in samples)
    # alpha (K*M) : the echo amplitudes
    '''
    From Helena's Mulan
    '''
    L = h.shape[0]
    M = h.shape[1]
    tau = np.zeros((K, M))
    alpha = np.zeros((K, M))
    for m in range(M):
        # Pick all local maxima
        loc_argmax = np.array([0, L-1])  # 1, L])
        for l in range(1, L-1):
            if(h[l-1, m] < h[l, m] and h[l+1, m] < h[l, m]):
                loc_argmax = np.append(loc_argmax, l)
        # Keep only K greatest
        loc_max = h[loc_argmax.astype(int), m]
        sorted_idx = (-loc_max).argsort()
        #ignore, sorted_idx = sort(loc_max,reverse = true)
        location = loc_argmax[sorted_idx[0:K]]
        weight = loc_max[sorted_idx[0:K]]
        tau[0:len(location), m] = location
        alpha[0:len(weight), m] = np.real(weight)
        # Sort in time:
        sorted_idx = tau[:, m].argsort()
        tau[:, m] = np.sort(tau[:, m])
        alpha[:, m] = alpha[sorted_idx, m]
    return tau.squeeze(), alpha.squeeze()


def resample(sound, old_fs, new_fs):
    # secs = len(x)/in_fs  # Number of seconds in signal X
    # samps = int(secs*out_fs)     # Number of samples to downsample
    # return sg.resample(x, samps)
    # resampled =  nnresample.resample(sound, int(new_fs), int(old_fs))
    # restor the amplitud
    # a = np.max(np.abs(sound))
    # resampled = a*resampled/np.max(np.abs(resampled))
    resampled = lr.resample(sound, old_fs, new_fs)
    return resampled
    

def same_lenght_pad_with_zeros(x,y):
        lx = x.shape[0]
        ly = y.shape[0]
        m = ly - lx
        if m == 0:
            return x, y
        elif m > 0:
            z = np.zeros_like(y)
            z[:lx] = x
            x = z
        elif m < 0:
            z = np.zeros_like(x)
            z[:ly] = y
            y = z
        return x, y

def lag_finder(y1, y2, fs=16000):
    n = len(y1)
    corr = sg.correlate(y2, y1, mode='same') \
            / np.sqrt(sg.correlate(y1, y1, mode='same')[int(n/2)] \
                    * sg.correlate(y2, y2, mode='same')[int(n/2)])

    delay_arr = np.linspace(-0.5*n/fs, 0.5*n/fs, n)
    delay = delay_arr[np.argmax(corr)]

    print('y2 is ' + str(delay) + ' behind y1')

    # import matplotlib.pyplot as plt
    # plt.plot(delay_arr, corr)
    # plt.show()

    return int(delay)
