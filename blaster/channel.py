import numpy as np
import scipy as sp
import pandas as pd
import pickle as pkl
import random
import matplotlib.pyplot as plt
import pyroomacoustics as pra

from src.file_utils import save_to_pickle

EPS = 1e-6

def anchor_hp(ch1, ch2):
    h1_0 = ch1.loc[0]
    h1_0f = ch1.toa[0]
    h2_0 = ch2.loc[0]
    h2_0f = ch2.toa[0]
    m = min([h1_0, h2_0])
    mf = min([h1_0f, h2_0f])
    idx = [h1_0, h2_0].index(m)
    idxf = [h1_0f, h2_0f].index(mf)
    ch1.loc = [int(x-m) for x in ch1.loc]
    ch2.loc = [int(x-m) for x in ch2.loc]
    ch1.toa = [x-mf for x in ch1.toa]
    ch2.toa = [x-mf for x in ch2.toa]
    if idx == 0:
        a = ch1.coeff[0]
    else:
        a = ch2.coeff[0]
    ch1.coeff = [x/a for x in ch1.coeff]
    ch2.coeff = [x/a for x in ch2.coeff]
    if ch1.rir is not None:
        ch1.tof = ch1.toa[0]
        ch1.rir = ch1.rir[m+1:]/a
    if ch2.rir is not None:
        ch2.tof = ch2.toa[0]
        ch2.rir = ch2.rir[m+1:]/a

    if idx == 0:
        return ch1, ch2
    else:
        return ch2, ch1

class ImageSource_Channel():
    def __init__(self, 
                 mics_position,
                 source_position=None,
                 absorption=0.5,
                 fs=16000,
                 do_compute_rir = False,
                 filter_length=None,
                 room_size=[6, 5, 4],
                 max_n_diracs=20):
        
        self.fs = fs
        self.filter_lenght = filter_length
        self.max_n_diracs = max_n_diracs
        self.room_size = room_size
        self.do_compute_rir = do_compute_rir
        
        max_order = get_next_order_for_n_echoes(max_n_diracs)

        room = pra.ShoeBox(
            self.room_size,
            absorption=absorption,
            fs=self.fs,
            max_order=max_order,
        )

        if source_position is None:
            source_position = np.random.random(3)*(np.array(room_size)*0.7 + 0.2)

        room.add_source(source_position)

        mic1 = mics_position[0]
        mic2 = mics_position[1]
        room.add_microphone_array(
            pra.MicrophoneArray(
                np.array([mic1, mic2]).T,
                room.fs)
        )

        # compute image sources
        t_offset = room.t0  # get_filter_delay

        images_df = pd.DataFrame()
        
        room.image_source_model(use_libroom=True)
        if do_compute_rir:
            room.compute_rir()
            rirs = room.rir

        images = room.sources[0]
        n_images = len(images.orders)

        assert n_images >= max_n_diracs

        n_mics = room.mic_array.R.shape[1]
        counter = 0
        
        for j in range(n_mics):
            mic = room.mic_array.R[:, j]
            for i in range(n_images):
                images_df.at[counter, 'mic'] = int(j)
                images_df.at[counter, 'orders'] = images.orders[i]
                images_df.at[counter, 'images_x'] = images.images[0, i]
                images_df.at[counter, 'images_y'] = images.images[1, i]
                images_df.at[counter, 'images_z'] = images.images[2, i]
                images_df.at[counter, 'walls'] = images.walls[i]
                dist = np.linalg.norm(images.images[:, i] - mic)
                images_df.at[counter, 'TOA'] = dist/343 + t_offset
                images_df.at[counter, 'absorption'] = images.damping[i]
                images_df.at[counter, 'damping'] = images.damping[i]/(4.*np.pi*dist)
                images_df.at[counter, 'distance'] = dist
                counter += 1
        
        toa_and_coeffs_dict = []
        for j in range(n_mics):
            images_mic_j_df = images_df.loc[
                (images_df['mic'] == j) & (images_df['TOA'] < self.filter_lenght/self.fs)]
            # sort for TOA
            images_mic_j_df = images_mic_j_df.sort_values(by=['TOA'])
            # only the first k diract
            images_mic_j_df = images_mic_j_df[:self.max_n_diracs]
            # get loc (in samples) and coeff
            toa = images_mic_j_df['TOA'].values
            coeff = images_mic_j_df['damping'].values
            mics_dict = {
                'id' : j,
                'toa': toa.tolist(),
                'coeff': coeff.tolist(),                
            }
            if self.do_compute_rir:
                mics_dict['rir'] = rirs[j][0]
            toa_and_coeffs_dict.append(mics_dict)
        self.toa_and_coeffs_dict = toa_and_coeffs_dict

    def get_loc_and_coeff_dict(self, mic=0):
        return self.toa_and_coeffs_dict[mic]


def get_discrete_toa(toa, fs):
    loc = [int(t*fs) for t in toa]
    return loc

class Channel():
    def __init__(self, coeff=[],
                 loc=[],
                 toa=[],
                 fs=16000,
                 filter_length=None,
                 pad_filter_lenght=1,
                 do_on_grid_filter=False,
                 mode=None, 
                 mode_param=0,
                 numpy_h=None,
                 csv_file=None, mic=0, order=2):

        self.fs = fs
        self.filter_lenght = filter_length
        self.pad_filter_lenght = pad_filter_lenght
        self.rir = numpy_h
        self.do_on_grid_filter = do_on_grid_filter

        # loc: [samples]
        #   position of the dirac on the sample grid
        # toa: [seconds]
        #   position of the dirac on the continuous time
        if len(loc)==0 and len(toa) > 0:
            # if toa are provided, then loc are compute
            # as ... see MULAN paper
            assert len(coeff) == len(toa)
            loc = get_discrete_toa(toa, self.fs)

        elif len(toa) == 0 and len(loc) > 0:
            # if loc are provide, then toa are simply
            # loc/fs
            assert len(coeff) == len(loc)
            toa = [l/self.fs for l in loc]
        elif len(toa) > 0 and len(loc) > 0:
            raise ValueError('Only one between loc or toa must be provided')

        self.coeff = coeff
        self.loc = [int(l) for l in loc]
        self.toa = toa
        self.tof = loc[0] if len(loc) > 0 else None
        if self.do_on_grid_filter:
            self.toa = [int(l)/fs for l in loc]

        if self.rir is not None:
            self.rir = self.rir[:self.loc[-1]]

        assert len(self.coeff) == len(self.loc)
        assert isinstance(self.coeff, list)
        assert isinstance(self.loc, list)
        assert isinstance(self.toa, list)

        if mode == 'rnd':
            if filter_length is None:
                raise ValueError(
                    'When rnd filter, please define filter_length in sample')
            if filter_length is None:
                raise ValueError(
                    'When rnd filter, please define mode parameter.')
            # random locations
            loc = np.cumsum(np.random.poisson(20, size=filter_length))
            self.loc = [l for l in loc if l < filter_length] + [filter_length]
            # random coefficient
            coeff = np.sort(np.random.random(len(self.loc)))[::-1]
            self.coeff = [c for c in coeff]

        else:
            if filter_length is not None:
                if filter_length < self.loc[-1]:
                    raise ValueError(
                        'Filter length smaller than the channel definition')
                self.coeff.append(0)
                self.loc.append(filter_length)
            else:
                if not len(self.loc) == 0:
                    self.filter_lenght = self.loc[-1] + self.pad_filter_lenght

        assert len(self.loc) == len(self.coeff)

    def __str__(self):
        string = 'ID\tLOC\tTOA\tCOEFF\n'
        print(self.loc)
        for l, loc in enumerate(self.loc):
            string += '%d\t%d\t%1.3f\t%1.3f\n' % (l, loc, loc/self.fs, self.coeff[l])
        return string

    def to_dict(self):
        d = {
            'fs': self.fs,
            'filter_lenght': self.filter_lenght,
            'coeff': self.coeff,
            'loc': self.loc,
            'toa': self.toa,
            'tof': self.tof,
            'rir': self.rir,
        }
        return d

    def are_too_close_tau(self, thr=None):
        if thr is None:
            thr = 1/self.fs
        tau = np.array(self.toa)
        n_close = len(np.where(np.diff(tau) <= thr)[0])
        return n_close > 0


    def get_time_filter(self, L=None, kernel='dirac', fs=None):
        if L is None:
            L = self.filter_lenght
            T = self.fs*L
        
        if self.rir is not None:
            return self.rir[:L]

        if kernel == 'dirac':
            rir = np.zeros(L)
            for t, c in zip(self.loc, self.coeff):
                if t >= L:
                    break
                rir[t] = c

        return rir

    #TODO Visualize both grid and off-grid filters
    def plot(self, kernel='dirac', label='Channel', linefmt='C0-', markerfmt='o'):
        plt.stem(self.toa, self.coeff[:-1],
                 linefmt=linefmt, markerfmt=markerfmt,
                 use_line_collection=True, label=label)
        plt.xlabel('Time [samples]')
        plt.ylabel('Amplitude')

    def save(self, filename):
        d = {
            'fs':self.fs,
            'filter_lenght':self.filter_lenght,
            'coeff':self.coeff,
            'loc':self.loc ,
            'toa':self.toa ,
            'tof':self.tof ,
            'rir':self.rir ,
        }
        save_to_pickle(d, filename)

def count_echoes_from_order_3D_shoebox(k):
    return (4*k**3 + 6*k**2 + 8*k + 3)//3


def get_next_order_for_n_echoes(n):
    for k in range(100):
        n_delta = count_echoes_from_order_3D_shoebox(k)
        if n_delta > n:
            return k
    return np.NaN


if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    Fs = 16000
    t_max = 100

    rir1 = Channel(mode='rnd', fs=Fs, filter_length=t_max)
    rir2 = Channel(mode='rnd', fs=Fs, filter_length=t_max)

    rir1.plot()
    rir2.plot()
    plt.legend(['rir1', 'rir2'])
    plt.show()
    pass
