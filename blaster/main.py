import os
import sys
import subprocess
import getpass
import sacred
import numpy as np
import matplotlib.pyplot as plt
import socket

from sacred import Experiment
from src.my_mongo import setup_connection

from src.braire import Oracle, Braire, anchor_hp, plot_dual_certificate
from src.dsp_utils import awgn, make_same_size, pick_echoes
from src.sota import Sota_algos, channel_initial_guess
from src.metrics import goodnees_of_fit
from src.source import Source
from src.channel import Channel, anchor_hp, ImageSource_Channel
from src.file_utils import make_dirs, save_to_json
from src.measure import Measure

from src.visualization.visualize import Visualizer


#####################
# Experiment observer

# experiment name
EXP_NAME = 'benchmark' # tuning, benchmark
# get commit id
git_commit_id = subprocess.check_output(["git", "describe", "--always"]).strip()
git_commit_id = str(git_commit_id).replace('b', '').replace("'", '')
# get hostname
hostname = socket.gethostname()
hostname = 'igrida' if 'igrida' in hostname else hostname
hostname = 'embuscade' if 'embuscade' in hostname else hostname
# set exp name
ex = Experiment('%s_%s_%s' % (EXP_NAME, hostname, git_commit_id))
# ex = setup_connection(ex, hostname)

RESULT_DIR = './results/'

@ex.config
def config():
    # run trial
    run_id = 0
    same_id_multi_run = 0
    Fs = 16000
    # algorithm parameters
    t_max = 300 # samples
    max_iter = 5
    algo_name = 'top-scipy'  # top-scipy, top-ista, four-ista, braire, mulan, crossrel
    lam = 0.001 # sparsity regularizer - the lower the sparer. For mulan it is the number of diracs you want to retrieve
    init = 'rnd'
    # channel parameters
    max_n_diracs = 7 # at most 20 diracs (lower than Tmax)
    absorption = .5
    do_on_grid_filter = False
    # geometry parameters
    src_mic_pos_id = same_id_multi_run
    # signal parameters
    signal = 'impulse'  # 'noise'  # impulse, noise, speech
    duration = 0.25 # seconds
    snr  = 200
    # other
    do_plot = False
    do_anchor_hp = True
    post_processing = False
    # metrics
    acc_thr = 2 # samples

def stop():
    print(".. and Nyarlathotep said: 'LET THERE BE A STOP!")
    sys.exit(0)
    return

@ex.capture
def get_info(_run, run_id):
    if 'igrida' in hostname:
        job_id = int(os.environ["OAR_JOB_ID"])
    else:
        job_id = 0
    run_dir = '%s' % run_id
    run_name = _run.experiment_info["name"]
    return run_name, run_id, job_id, run_dir

@ex.capture
def load_data(signal, duration, snr, t_max, Fs, max_n_diracs, do_anchor_hp,
              src_mic_pos_id, absorption, do_on_grid_filter, do_plot):
        
    # source signal: impulse, noise, speech
    src = Source(signal, src_mic_pos_id, duration, Fs)

    # filter signal: ideal, synthetic, real
    src_pos = np.loadtxt('./data/processed/src_pos.csv')[:, src_mic_pos_id]
    mic1_pos = np.loadtxt('./data/processed/mic1_pos.csv')[:, src_mic_pos_id]
    mic2_pos = np.loadtxt('./data/processed/mic2_pos.csv')[:, src_mic_pos_id]
    mics_pos = [mic1_pos, mic2_pos]
    im_channel = ImageSource_Channel(
        source_position=src_pos, 
        mics_position=mics_pos,
        absorption=absorption,
        fs=Fs, filter_length=t_max, max_n_diracs=max_n_diracs)
    rir1_toa_coeff_dict = im_channel.get_loc_and_coeff_dict(mic=0)
    rir2_toa_coeff_dict = im_channel.get_loc_and_coeff_dict(mic=1)

    #TODO: implement sync kernel
    rir1 = Channel(
        coeff=rir1_toa_coeff_dict['coeff'], 
        toa=rir1_toa_coeff_dict['toa'],
        # numpy_h=rir1_toa_coeff_dict['rir'],
        filter_length=t_max,
        do_on_grid_filter=do_on_grid_filter,
    )
    rir2 = Channel(
        coeff=rir2_toa_coeff_dict['coeff'],
        toa=rir2_toa_coeff_dict['toa'],
        # numpy_h=rir2_toa_coeff_dict['rir'],
        filter_length=t_max,
        do_on_grid_filter=do_on_grid_filter,
    )

    # if 'fulgence' in hostname:
    #     rir1 = Channel(coeff=[1, .8, .6], loc=[0, 40, 120], fs=Fs, filter_length=t_max) # location in samples
    #     rir2 = Channel(coeff=[0.8, .7], loc=[38, 130], fs=Fs, filter_length=t_max) # location in samples

    print(rir1)
    print(rir2)

    if do_anchor_hp:
        rir1, rir2 = anchor_hp(rir1, rir2)

    if do_plot:
        rir1.plot()
        rir2.plot()
        plt.legend(['h_1', 'h_2'])
        plt.title('filters')
        plt.show()

    # observed signal
    print("-- convolving signals with filters at higher Fs...")
    m1 = src.convolve(ch=rir1, fs=1024.e3)
    m2 = src.convolve(ch=rir2, fs=1024.e3)
    print("done.")

    plt.plot(m1, label='resampled h1')
    plt.plot(m2, label='resampled h2')
    plt.stem(np.array(rir1.toa)*Fs,
             rir1.coeff[:-1], 'C0', label='true echoes h1')
    plt.stem(np.array(rir2.toa)*Fs,
             rir2.coeff[:-1], 'C1', label='true echoes h2')
    plt.legend()
    plt.show()

    m1, m2 = make_same_size(m1, m2)
    
    # add noise
    m1 = awgn(m1, snr)
    m2 = awgn(m2, snr)

    assert m1.size == m2.size

    return rir1, rir2, m1, m2, src

@ex.capture
def run_main_with_params(_run, seed, run_id, t_max, Fs, algo_name, init, lam, max_iter, do_plot, post_processing, same_id_multi_run, acc_thr, do_anchor_hp):

    exp_name, exp_id, exp_job_id, exp_dir = get_info()
    print('Running', exp_name, 'id', exp_id)

    # path for saving artifacts
    path_to_artifacts = RESULT_DIR + exp_name + '/' + exp_dir + '/'
    make_dirs(path_to_artifacts)

    # save name of the job id
    with open(path_to_artifacts + "%d.txt" % exp_job_id, "w") as text_file:
        text_file.write("Job id: %d" % exp_job_id)

    # save config
    save_to_json(_run.config, path_to_artifacts + 'config.json')

    # load data
    rir1, rir2, m1, m2, src = load_data()

    rir1.save(path_to_artifacts+'ch1_ref.pkl')
    rir2.save(path_to_artifacts+'ch2_ref.pkl')

    if init == 'ref':
        init = {'h1': rir1.get_time_filter(L=t_max),
                'h2': rir2.get_time_filter(L=t_max)}
    elif init == 'rnd':
        init = channel_initial_guess(init, t_max, (m1, m2))
    else:
        pass

    if algo_name == 'braire':
        print('braire here')
        algo = Braire(t_max, Fs, lam, max_iter, False, post_processing=post_processing)
        #plot_dual_certificate(algo, x1, x2, (0, t_max/Fs), (0, t_max / Fs), rir1.loc, rir2.loc, lam, Fs)
        h1, h2 = algo.run(m1, m2)

    elif algo_name == 'braire-path':
        print('braire here')
        algo = Braire(m1, m2, t_max, Fs, max_iter, False, post_processing=post_processing)

        init = Measure([], [], [], [])

        nbLbd = 1.
        while (init.nb_set1()+1 < max_n_diracs) and (init.nb_set2() < max_n_diracs):
            #-- 1. Compute lambda
            lam = 10**(-0.025 * nbLbd)

            #-- 2. Extracting futur measure
            channels_hat = algo.run(lam, mesInit=init)

            #-- 3. Extracting futur initialization
            init = Measure(
                h1.toa.copy(),
                h2.toa.copy(),
                h1.coeff.copy(),
                h2.coeff.copy()
            )

            #-- 4. Removing the anchor constraints
            del init.support_1[0]
            del init.coeff_1[0]

            #-- 5. increase counter
            nbLbd += 1

    else:
        algo = Sota_algos(algo_name, t_max, Fs, lam, max_iter, do_plot)
        h1, h2 = algo.estimate(m1, m2, init)

    if do_anchor_hp:
        h1, h2 = anchor_hp(h1, h2)
        rir1, rir2 = anchor_hp(rir1, rir2)

    print("-- Initial room impule response")
    print(np.array(rir1.toa))
    print(np.array(rir2.toa))
    print("")
    print("-- Estimated room impule response")
    print(np.round(np.array(h1.toa), 6))
    print(np.round(np.array(h2.toa), 6))
    print("")


    print('REF RIR1')
    print(rir1)
    print('EST RIR1')
    print(h1)

    print('REF RIR2')
    print(rir2)
    print('EST RIR2')
    print(h2)

    # information about the algorithm
    fun = algo.fun # value of the objective function
    it = algo.it   # converged iteration
    
    converged = algo.converged  # if it has converged to a solutios
    dual_gap = algo.dual_gap

    # we want 1D array with the timestamps
    appms, ps, rs\
        = goodnees_of_fit(
            [np.array(rir1.toa), np.array(rir2.toa)],
            [np.array(h1.toa), np.array(h2.toa)],
            'appm', appm_thr=acc_thr/Fs
        )
    appm = np.mean(appms)

    print(':: appm results ::')
    print('-- F:', np.mean(appms))
    print('-- P:', np.mean(ps))
    print('-- R:', np.mean(rs))

    # save artifacts
    h1.save(path_to_artifacts+'ch1_est.pkl')
    h2.save(path_to_artifacts+'ch2_est.pkl')

    # result dict
    results = {
        "algo.function": fun,
        "algo.converged": 'converged' if converged else 'not',
        "algo.iteration": it,
        "results.f1": appms[0],
        "results.f2": appms[1],
        "results.f": np.mean(appms),
        "results.p1": ps[0],
        "results.p2": ps[1],
        "results.p": np.mean(ps),
        "results.r1": rs[0],
        "results.r2": rs[1],
        "results.r": np.mean(rs),
    }
    print(results)
    save_to_json(results, path_to_artifacts + 'result.json')


    # report results
    _run.log_scalar("algo.function", fun)
    _run.log_scalar("algo.converged", converged)
    _run.log_scalar("algo.iteration", it)

    _run.log_scalar("results.f1", appms[0])
    _run.log_scalar("results.f2", appms[1])
    _run.log_scalar("results.f", np.mean(appms))
    _run.log_scalar("results.p1", ps[0])
    _run.log_scalar("results.p2", ps[1])
    _run.log_scalar("results.p", np.mean(ps))
    _run.log_scalar("results.r1", rs[0])
    _run.log_scalar("results.r2", rs[1])
    _run.log_scalar("results.r", np.mean(rs))


    _run.add_artifact(path_to_artifacts+'ch1_ref.pkl', 'ch1_ref')
    _run.add_artifact(path_to_artifacts+'ch1_est.pkl', 'ch1_est')
    _run.add_artifact(path_to_artifacts+'ch2_ref.pkl', 'ch2_ref')
    _run.add_artifact(path_to_artifacts+'ch2_est.pkl', 'ch2_est')

    # print results
    print('Cost function for', algo_name, ':', fun)
    print('Goodness of Fit:')
    print(' -- APPM:', appm)

    # show results
    viz = Visualizer()
    viz.plot_channel_estimation_results(
        ref=[rir1, rir2], est=[h1, h2],
        path_to_output=path_to_artifacts,
        sacred_run = _run,
        do_plot=do_plot
    )


@ex.automain
def main():
    run_main_with_params()
