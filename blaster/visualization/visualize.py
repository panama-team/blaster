import numpy as np
import matplotlib.pyplot as plt
from src.file_utils import make_dirs


def count_nearest(array, value, thr):
    array = np.asarray(array)
    c = len(array[np.abs(array - value) <= thr])
    return c

class Visualizer():
    def __init__(self):
        pass

    def plot_channel_estimation_results(
            self, ref=[], est=[],
            path_to_output='./results/',
            title = '',
            complete_path_and_filename = None, sacred_run=None, do_plot=False, thr=2):

        try:
            n_chan = len(ref)
            fig, ax = plt.subplots(n_chan, 1, sharex=True)

            for c in range(n_chan):

                ch_ref = ref[c]
                ch_est = est[c]

                toa_ref, coeff_ref = ch_ref.toa, ch_ref.coeff
                toa_est, coeff_est = ch_est.toa, ch_est.coeff

                coeff_est = coeff_est/np.max(np.abs(coeff_est))
                coeff_ref = coeff_ref/np.max(np.abs(coeff_ref))

                # plot ground truth
                ax[c].stem(toa_ref, coeff_ref[:len(toa_ref)],
                           linefmt='C0-', markerfmt='o',
                           use_line_collection=True, label="GroudTruth")

                # # plot multeplicity
                # mul = np.where(np.diff(toa_ref) < thr)[0]

                # for m in mul:
                #     ax[c].text(toa_ref[m], coeff_ref[m], str('2'))

                # plot estimation
                ax[c].stem(toa_est, coeff_est[:len(toa_est)],
                           linefmt='C1-', markerfmt='X',
                           use_line_collection=True, label='Estimated')

                ax[c].legend()
                ax[c].set_title('Channel %d'%(c))

            plt.suptitle(title)

            if complete_path_and_filename is None:
                path_to_output_figure = path_to_output + 'channels_estimation.pdf'
            else:
                path_to_output_figure = complete_path_and_filename

            plt.savefig(path_to_output_figure)

            # print(path_to_output_figure)
            if do_plot:
                plt.show()

            if sacred_run is not None:
                sacred_run.add_artifact(path_to_output, 'ch_plot')
        except Exception as e:
            print(e)
            print('... do not care and skip it!')

        plt.close()
        return
