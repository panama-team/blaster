import numpy as np

import matplotlib.pyplot as plt

from src.measure import Measure
from src.modelisation import ObservationOperator

class Blasso_problem(object):
    """docstring for BlassoPb"""
    def __init__(self, opObs, yobs, lbd, maxit):
        """
        """
        super(Blasso_problem, self).__init__()

        self.opObs = opObs
        self.yobs = yobs
        self.lbd = lbd
        self.maxit = maxit
        self.vec_cost_func = np.nan * np.zeros(maxit+1)


    def update_it(self, it, bsh, measure):
        """
        Inputs:
            - it: int
            current iteration
            
            - bsh: np.array
            vector of size m corresponding to A\\mu
            
            - measure: instance of Measure 

        """
        assert(it <= self.maxit)

        self.vec_cost_func[it] = self.cost_function(measure, bsh=bsh)


    def cost_function(self, measure, bsh=None):
        """
        Compute the cost function 
        $$  .5 * || y - A\\mu ||_2^2 + \\lambda || \\mu ||_{TV}  $$

        if bsh is None, it is recomputed from easure
        """

        if bsh is None:
            bsh = self.opObs.compute_observation(measure)

        out = .5 * np.linalg.norm(self.yobs - bsh)**2
        out += self.lbd * (np.linalg.norm(np.array(measure.coeff_1), 1) + np.linalg.norm(np.array(measure.coeff_2), 1))

        return out


    def plot(self):
        """
        plots figure of merits:
            - evolution of cost function
        """

        fig, ax = plt.subplots(1,1, figsize=(16,9), sharex=True, sharey=True)
        ax.plot(np.arange(self.maxit+1), self.vec_cost_func, 'rx')

        ax.set_title("value of the cost function")

        plt.show()


