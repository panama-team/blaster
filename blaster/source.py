import numpy as np
import soundfile as sf
import matplotlib.pyplot as plt

from src.dsp_utils import awgn, resample

path_to_timit = './data/raw/timit/'

class Source():
    
    def __init__(self, signal, src_mic_pos_id, duration, fs, position=None):

        self.signal = signal
        self.n_sec = duration
        self.fs = float(fs)
        self.n_smpl = int(duration*fs)
        self.xyz = position

        t = np.arange(self.n_smpl) # time support (in samples)

        if signal == 'impulse':
            self.values = np.array([1.])
        elif signal == 'sine':
            self.values = np.sin(2*np.pi*t/440)  # sine signal
        elif signal == 'noise':
            self.values = np.random.random(self.n_smpl) - 0.5
        elif signal == 'speech':
            idx = str(src_mic_pos_id)
            wav, fs = sf.read(path_to_timit + idx + '.wav')
            if not fs == self.fs:
                raise ValueError('Not implemented for different Fs')
            self.n_smpl = np.min([self.n_smpl, wav.size])
            self.n_sec = self.n_smpl/self.fs
            self.values = wav[:self.n_smpl]
        else:
            raise ValueError("NoSignalDefinitionError")


    def convolve(self, ch, fs=None):
        if fs is not None:
            x = self.values.copy()
            # resample source signal
            x = resample(x, self.fs, fs)
            # get filter toa and coef
            h_toa = [ int(t*fs) for t in ch.toa]
            h_coeff = ch.coeff
            h = np.zeros(int(ch.filter_lenght/ch.fs*fs))
            for t, c in zip(h_toa, h_coeff):
                h[t] = c
            h = h[:h_toa[-1]+10]
            y = np.convolve(x, h)
            y = resample(y, fs, self.fs)
        else:
            h = ch.get_time_filter()
            y = np.convolve(self.values, h)
        return y


    def __str__(self):
        return 'Source: %s (%1.2f sec at %d Hz)'%(self.signal, self.n_sec, self.fs)


    # def plot(self):
    #     time = np.arange(self.n_smpl)/self.fs
    #     plt.plot(time, self.values)
