import numpy as np
import scipy as sp
import pandas as pd
import time

from copy import deepcopy as cp
from scipy.optimize import minimize, minimize_scalar

import matplotlib.pyplot as plt

from src.dsp_utils import DFT_matrix, apply_DFT
from src.dictionary import ObservationOperator
from src.atome_selection import InnerProductResidual
from src.measure import Measure
from src.lasso import Complex_nn_lasso
from src.channel import Channel, anchor_hp
from src.problem import Blasso_problem

EPS_MAX_DUAL = 1e-1
EPS_MERGE    = 1e-4
NB_NEGATIVE_SPIKES = 3

class Oracle():
    def __init__(self, max_iter, channels):
        self.max_iter = max_iter
        self.channels = channels
        assert len(channels) == 2

    def get_locations(self, it):
        # location index and iteration are equal here
        if it > self.max_iter:
            raise ValueError('Max iteration reached')
        return (h.loc[it] for h in self.channels)

def sort_coeff_and_loc_list(loc, coeff):
    loc = np.array(loc)
    coeff = np.array(coeff)
    sort_indeces = np.argsort(loc)
    loc = loc[sort_indeces]
    coeff = coeff[sort_indeces]
    return list(loc), list(coeff)


class Blaster():
    """ Our implementation of the ... problem
    """

    def __init__(self, x1, x2, L, Fs, max_iter, normalizeDic=True, do_plot=False, post_processing=False):
        """
        Parameters
        ----------
        x1 : np.ndarray
            signal from microphone 1
            size []
        x2 : np.ndarray
            signal from microphone 2
            size []
        L : positive integer
            lenght of the desired filter
        Fs : positive float
            sampling frequency
        max_iter : positive integer
            maximum number of iterations for Blaster
            (should be overestimated)
        normalizeDic : bool
            use normalized dictionary, that is each atom is unit-norm
            (default is True)
        do_plot : bool
            flag for ploting internal optimization results
            (default is False)
        post_processing : 
            post-precess outputs
            (default is False)
        """
        self.algorithm = "blaster"
        self.max_iter = max_iter
        self.L = L # lenght of the desired filter
        self.Fs = Fs
        self.fun = np.infty
        self.it = 0
        self.converged = False

        self.normalizeDic = normalizeDic
        self.isNonNegative = True

        self.do_plot = do_plot
        self.post_processing = post_processing

        self.t_max = float(self.L) / float(self.Fs)

        epsilon_T1 = 20. / float(self.Fs)# t_max / 50.
        self.T1 = (epsilon_T1, self.t_max)
        self.T2 = (0, self.t_max)

        #-- 1. Fourier matrix transformations
        self.bfa1 = apply_DFT(x1, Te=1./float(self.Fs))  # Nx1
        self.bfa2 = apply_DFT(x2, Te=1./float(self.Fs))  # Nx1

        #-- 2. Observation operator
        obsOp = ObservationOperator(self.bfa1, self.bfa2, self.Fs, 
            self.T1, self.T2, normalizeDic=self.normalizeDic)
        self.obsOp = obsOp

        #-- 3. Observation
        anchor = Measure([0.], [], [1.], [])
        yObs = - obsOp.compute_observation(anchor)
        self.yObs = yObs

        #-- 4. Compute lambda_max
        dualcertif = InnerProductResidual(obsOp, self.yObs, 1., nnegative=self.isNonNegative)
        (tnew, etamax, a) = dualcertif.maximize()
        self.lambda_max = etamax


    def run(self, frac_lam, mesInit=Measure([], [], [], [])):
        """ Solve the channel estimation problem

        Parameters
        ----------
        frac_lam : float 
            normliazed value of the regularization parameter
            such that lambda = frac_lam * lambda_max
            should be in between 0 and 1
        mesInit : Measure
            Initialization value for the measure
            (default is the empty measure)

        Returns
        -------
        channel_hat : Measure
            estimated channels
        """

        assert(frac_lam >= 0)

        return self._run_impl(frac_lam, mesInit)


    def _run_impl(self, frac_lam, mesInit):
        """ Internal implementation of run

        Parameters
        ----------
        frac_lam : float 
            normliazed value of the regularization parameter
            such that lambda = frac_lam * lambda_max
            should be in between 0 and 1
        mesInit : Measure
            Initialization value for the measure

        Returns
        -------
        channel_hat : Measure
            estimated channels
        """

        #-- get lambda from lambda_max
        lam = frac_lam * self.lambda_max

        print("Starting blaster with:")
        print("   - t_max = " + str(self.t_max))
        print("   - lbd = " + str(lam))
        print("")

        #-- Making a copy to avoid ambiguities
        channel_hat = Measure(
            mesInit.support_1.copy(),
            mesInit.support_2.copy(),
            mesInit.coeff_1.copy(),
            mesInit.coeff_2.copy()
        )
        bsh = self.obsOp.compute_observation(channel_hat)

        blasso_pb = Blasso_problem(self.obsOp, self.yObs, lam, self.max_iter)
        blasso_pb.update_it(0, bsh, channel_hat)

        # 1. Starting algorithm
        it = 0
        converged = False
        while it < self.max_iter:

            print("#####################")
            print("#    Iteration " + str(it+1))
            print("#####################")
            print("")

            # --1a. Create Dual certficate and maximize
            dualcertif = InnerProductResidual(self.obsOp, self.yObs - bsh, lam, nnegative=self.isNonNegative)
            
            (tnew, etamax, a) = dualcertif.maximize()

            if np.abs(etamax) <= 1 + EPS_MAX_DUAL:
                # -- Meaning that || \eta ||_\infty <= 1, the algorithm has converged
                print("||\\eta||_\\infty <= 1")
                print("")
                converged = True
                break

            print('-- New spike found')
            if a == 1:
                print("   Maximal position in T1 at " + str(tnew))
            elif a == 2:
                print("   Maximal position in T2 at " + str(tnew))
            else:
                raise Exception("Wrong value for a")

            print("   Dual certificate value " + str(etamax))
            print("")
            
            if self.do_plot:
                dic_plot = {"maximizer":(tnew, a), "suptitle":"Iteration " + str(it)}
                dualcertif.plot(channel_hat, dic_plot=dic_plot)


            # --1b. detect if the new spike is too close to an existing one
            i_close = channel_hat.is_new_spike_close(tnew, a, epsilon=EPS_MERGE)
            if i_close is not None:
                # if they are close to each other: merge and optimize
                channel_hat = self._optimize_parameter(self.yObs, self.obsOp, lam, channel_hat, a, i_close, self.T1, self.T2)
            
            else:
                # Otherwise append it
                channel_hat.append(a, tnew, 0.)


            # --1c. Lasso update of the coefficients
            channel_hat = self._lasso_update(self.yObs, self.obsOp, lam, channel_hat)


            # --1d. clean location from h1 and h2
            channel_hat.remove_unused_spike()


            # --1e. joint optimization
            channel_hat = self._joint_update(self.yObs, self.obsOp, lam, \
                    channel_hat, self.T1, self.T2)


            # --2f. Eventually merge spikes
            channel_hat = self.detect_and_process_closed_spikes(self.yObs, self.obsOp, lam, \
                channel_hat, self.T1, self.T2)


            #-- 3. Do something with negative spikes
            if channel_hat.nb_negative_spikes() > NB_NEGATIVE_SPIKES:
                channel_hat = self.post_process_output(channel_hat)

            # --1f. Finale update
            print('-- End of iteration')
            channel_hat.pretty_print()


            bsh = self.obsOp.compute_observation(channel_hat)
            it += 1

            blasso_pb.update_it(it, bsh, channel_hat)


        print("#####################")
        print("# End of algorithm   ")
        print("#####################")
        print("")

        # -- final plots
        dualcertif = InnerProductResidual(self.obsOp, self.yObs - bsh, lam, nnegative=self.isNonNegative)

        if self.do_plot:
            dic_plot = {"suptitle":"Dual certificate"}
            dualcertif.plot(channel_hat, dic_plot=dic_plot)

            blasso_pb.plot()

        # -- Misc
        self.fun = blasso_pb.cost_function(channel_hat, bsh=bsh)
        self.dual_gap = 240191 #TODO
        self.converged = converged


        # --post processing (if necessary)
        if self.post_processing:
            channel_hat = self.post_process_output(channel_hat)

        # --Finaly append the spike at 0 in channel 1
        channel_hat.append(1, 0., 1.)

        # if len(channel_hat.support_1) == 0 or len(channel_hat.support_2) == 0:
        #     raise ValueError('One of the two channel is empty')

        # -- sort peaks and locs
        channel_hat.sort_coeff_and_loc_list()

        return channel_hat


    def _lasso_update(self, yObs, obsOp, lam, channel):
        """ Given location, estimate the coefficients

        Given an existing set of locations, estimate the coefficients by 
        solving a finite dimentional nonnegative Lasso problem

        Parameters
        ----------
        yObs : np.ndarray
            observation vector
            size [m]
        opsOp : dictionary.ObservationOperator
            Observation op related to the problem
        lam : positive float
            value of the regularization parameter
        channel : measure.Measure
            contains the locations of spikes

        Returns
        -------
        channel_hat : measure.Measure
            channel with same location as input but
            values of the coefficients have benn updated
        """
        k = yObs.shape[0]

        n1 = channel.nb_set1()
        n2 = channel.nb_set2()

        A = np.zeros((k, n1 + n2), dtype=complex)
        for i in range(n1):
            A[:, i] = obsOp.signed_atome_T1(channel.support_1[i])

        for i in range(n2):
            A[:, n1 + i] = obsOp.signed_atome_T2(channel.support_2[i])

            # Then Lasso update
        lasso = Complex_nn_lasso(yObs, A, lam)
        coeffs = lasso.run(maxit=500000, gaptol=1e-6)


        channel.coeff_1 = coeffs[:n1].tolist()
        channel.coeff_2 = coeffs[n1:].tolist()

        print('-- Lasso update')
        print("   lbd: " + str(lam))
        print("   gap: " + str(lasso.dual_gap))
        channel.pretty_print()

        return channel


    def _optimize_parameter(self, yObs, obsOp, lam, channel, iset, imerge, T1, T2):

        if iset == 1:
            channel.coeff_1[imerge] = 0.
        else:
            channel.coeff_2[imerge] = 0.

        bsh = obsOp.compute_observation(channel)
        dualcertif = InnerProductResidual(obsOp, yObs - bsh, lam, nnegative=True)

        if iset == 1:
            func = lambda t: - np.abs(dualcertif._evaluate_set1(t))
            res = minimize(func, [channel.support_1[imerge]], bounds=[T1], method='L-BFGS-B')
            channel.support_1[imerge] = res.x[0]
        else:
            func = lambda t: - np.abs(dualcertif._evaluate_set2(t))
            res = minimize(func, [channel.support_2[imerge]], bounds=[T2], method='L-BFGS-B')
            channel.support_2[imerge] = res.x[0]

        return channel


    def _joint_update(self, yObs, obsOp, lam, channel, T1, T2):
        """ Joint update of locations and coefficients

        Joint update of locations and coefficients using
        a few gradient steps

        Parameters
        ----------
        yObs : np.ndarray
            observation vector
            size [m]
        obsOp : dictionary.ObservationOperator
            Observation op related to the problem
        lam : positive float
            value of the regularization parameter
        channel : measure.Measure
            contains the locations of spikes and coefficients
        T1 : list of two elements
            admissible values for locations in channel 1
        T2 : list of two elements
            admissible values for locations in channel 2

        Returns
        -------
        channel_hat : measure.Measure
            channel where locations and coefficients have
            been updated
        """

        n1 = channel.nb_set1()
        n2 = channel.nb_set2()
        k = yObs.shape[0]

        # --1. define objective function
        def objective_func(var):
            return .5 * np.linalg.norm(\
                yObs - obsOp.compute_observation(Measure(\
                    var[:n1], var[n1:(n1+n2)], \
                    var[(n1+n2):(2*n1+n2)], var[(2*n1+n2):])) \
                    , 2)**2 \
                + lam * np.linalg.norm(var[(n1+n2):], 1)

        x0 = np.array(channel.support_1 + channel.support_2 + channel.coeff_1 + channel.coeff_2).reshape(2*(n1+n2))

        if self.isNonNegative:
            bnds = tuple([T1 for i in range(n1)] + [T2 for i in range(n2)] \
                + [(0., 10.)  for i in range(n1+n2)])

        else:
            bnds = tuple([T1 for i in range(n1)] + [T2 for i in range(n2)] \
                + [(-10., 10.)  for i in range(n1+n2)])

        res = minimize(objective_func, x0, tol=1e-6, bounds=bnds, method='SLSQP')

        channel.support_1 = res.x[:n1].tolist()
        channel.support_2 = res.x[n1:(n1+n2)].tolist()

        channel.coeff_1 = res.x[(n1+n2):(2*n1+n2)].tolist()
        channel.coeff_2 = res.x[(2*n1+n2):].tolist()

        print('-- Joint update')
        channel.pretty_print()

        return channel


    def detect_and_process_closed_spikes(self, yObs, obsOp, lbd, \
                channel, T1, T2):
        """
        """

        # Take care of channel 1:
        n1 = len(channel.support_1)
        for counter in range(n1):

            # Remove last spike and coeffs
            t = channel.support_1.pop()
            coeff = channel.coeff_1.pop()

            # Check if spike is clone to another
            i_close = channel.is_new_spike_close(t, 1, epsilon=EPS_MERGE)
            if i_close is not None:
                # if they are close to each other: merge and optimize
                channel = self._optimize_parameter(yObs, obsOp, lbd, channel, 1, i_close, T1, T2)

                # Lasso update of the coefficients
                channel = self._lasso_update(yObs, obsOp, lbd, channel)

            else:
                # The spike is not close to another - add it to the channel
                channel.support_1.insert(0, t)
                channel.coeff_1.insert(0, coeff)


        # Same for channel 2:
        n2 = len(channel.support_2)
        for counter in range(n2):

            # Remove last spike and coeffs
            t = channel.support_2.pop()
            coeff = channel.coeff_2.pop()

            # Check if spike is clone to another
            i_close = channel.is_new_spike_close(t, 2, epsilon=EPS_MERGE)
            if i_close is not None:
                # if they are close to each other: merge and optimize
                channel = self._optimize_parameter(yObs, obsOp, lbd, channel, 2, i_close, T1, T2)

                # Lasso update of the coefficients
                channel = self._lasso_update(yObs, obsOp, lbd, channel)

            else:
                # The spike is not close to another - add it to the channel
                channel.support_2.insert(0, t)
                channel.coeff_2.insert(0, coeff)


        return channel


    def post_process_output(self, channel):
        print('Post processing channel')

        #1. remove negative spike
        idxneg1 = [i for i in range(channel.nb_set1()) if channel.coeff_1[i] < 0]
        idxneg2 = [i for i in range(channel.nb_set2()) if channel.coeff_2[i] < 0]

        channel.remove_spikes(idxneg1, idxneg2)
        channel.pretty_print()

        #2. remove spikes with small amplitude
        if channel.nb_set1() > 0:
            maxamp1 = np.max(np.array(channel.coeff_1))
            maxamp1 = max(maxamp1, 1)
            idxamp1 = [i for i in range(channel.nb_set1()) if maxamp1 / channel.coeff_1[i] >= 50]
        else:
            idxamp1 = None

        if channel.nb_set2() > 0:
            maxamp2 = np.max(np.array(channel.coeff_2))
            idxamp2 = [i for i in range(channel.nb_set2()) if maxamp2 / channel.coeff_2[i] >= 50]
        else:
            idxamp2 = None

        channel.remove_spikes(idxamp1, idxamp2)

        return channel

