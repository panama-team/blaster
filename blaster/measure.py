import numpy as np

class Measure(object):
    """ Measure class
        
        Class representing a signal :math:`\\mu` which is a sum of spikes

        Attributes
        ----------
        support_1 : list
            list of position of spikes
        support_2 : list
            list of position of spikes
        coeff_1 : list
            list of coefficients
        coeff_2 : list
            list of coefficients
    """

    def __init__(self, support_1, support_2, coeff_1, coeff_2):
        """
        Parameters
        ----------
        support_1 : list
            list of position of spikes
        support_2 : list
            list of position of spikes
        coeff_1 : list
            list of coefficients
        coeff_2 : list
            list of coefficients
        """

        super(Measure, self).__init__()
        
        self.support_1 = support_1
        self.support_2 = support_2
        self.coeff_1 = coeff_1
        self.coeff_2 = coeff_2
        

    def append(self, iset, tnew, coeff):
        """ Add a new spike to one of the two channel

        Parameters
        ----------
        iset : int
            set to which belongs the new spike
        tnew : float
            location of the new spike
        coeff : float
            coefficient associated to new spike
        """

        if iset == 1:
            self.support_1.append(tnew)
            self.coeff_1.append(coeff)

        elif iset == 2:
            self.support_2.append(tnew)
            self.coeff_2.append(coeff)


    def nb_set1(self):
        """ Returns the number of spikes in channel 1

        Returns
        -------
        nb_spikes : int
            number of spikes in channel 1
        """
        return len(self.support_1)


    def nb_set2(self):
        """ Returns the number of spikes in channel 2

        Returns
        -------
        nb_spikes : int
            number of spikes in channel 2
        """
        return len(self.support_2)


    def nb_negative_spikes(self):
        """
        Count the number of negative spikes

        Parameters
        ----------

        Returns
        -------
        n : int
            number of spikes associated
            to negative coefficient
        """
        n1 = np.sum( np.array(self.coeff_1) < 0 )
        n2 = np.sum( np.array(self.coeff_2) < 0 )

        return n1 + n2


    def is_new_spike_close(self, tnew, settnew, epsilon=0):
        """ Check if a new spike is closed to an existing one

        Parameters
        ----------
        tnew : float
            location of the new spike
        settnew : int
            set to which belong the new spike
        epsilon : float
            tolerance for closeness
            (default=0)
        Returns
        -------
        is_closed : bool
            True if close to an existing spike 
        """

        if settnew == 1 and len(self.support_1) > 0 and np.min(np.abs(tnew - np.array(self.support_1))) < epsilon:
            i_merge = np.argmin(np.abs(tnew - np.array(self.support_1)))

            return i_merge

        elif settnew == 2 and len(self.support_2) > 0 and np.min(np.abs(tnew - np.array(self.support_2))) < epsilon:
            i_merge = np.argmin(np.abs(tnew - np.array(self.support_2)))

            return i_merge

        return None


    def remove_unused_spike(self):
        """ Remove spikes associated to zero coefficients in both channel
        """

        n1 = self.nb_set1()
        n2 = self.nb_set2()

        self.support_1 = [self.support_1[i] for i in range(n1) if self.coeff_1[i] != 0]
        self.coeff_1   = [self.coeff_1[i] for i in range(n1) if self.coeff_1[i] != 0]

        self.support_2 = [self.support_2[i] for i in range(n2) if self.coeff_2[i] != 0]
        self.coeff_2   = [self.coeff_2[i] for i in range(n2) if self.coeff_2[i] != 0]



    def remove_spikes(self, idxs1=None, idxs2=None):
        """
            remove spikes whose indexes are in lists

        Parameters
        ----------
        idxs1 : list of integers or None
            If not None, list of indexes which are to be removed in channel 1
            (default is None)
        idxs2 : list of integers or None
            If not None, list of indexes which are to be removed in channel 2
            (default is None)
        """

        if idxs1 is not None:
            n1 = self.nb_set1()
            self.support_1 = [self.support_1[i] for i in range(n1) if i not in idxs1]
            self.coeff_1   = [self.coeff_1[i]   for i in range(n1) if i not in idxs1]

        if idxs2 is not None:
            n2 = self.nb_set2()
            self.support_2 = [self.support_2[i] for i in range(n2) if i not in idxs2]
            self.coeff_2   = [self.coeff_2[i]   for i in range(n2) if i not in idxs2]


    def sort_coeff_and_loc_list(self):
        """ Sort spikes according to their location
        """

        idx1 = np.argsort(np.array(self.support_1))
        self.support_1 = [self.support_1[i] for i in idx1]
        self.coeff_1   = [self.coeff_1[i] for i in idx1]

        idx2 = np.argsort(np.array(self.support_2))
        self.support_2 = [self.support_2[i] for i in idx2]
        self.coeff_2   = [self.coeff_2[i] for i in idx2]


    def pretty_print(self):
        """ Nice print function of the location
        """

        self.sort_coeff_and_loc_list()

        try:
            print("Filter 1")
            print(" loc:   " + str(np.round(np.array(self.support_1), 4)))
            print(" coeff: " + str(np.round(np.array(self.coeff_1), 4)))

            print("Filter 2")
            print(" loc:   " + str(np.round(np.array(self.support_2), 4)))
            print(" coeff: " + str(np.round(np.array(self.coeff_2), 4)))

        except OSError:
            print("Something wrong happens while printing... Sorry for that")

        print(" ")
