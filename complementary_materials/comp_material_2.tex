%!TEX root = ./comp_material.tex
%!TEX spellcheck = en_US

\section{Sliding Frank-Wolfe algorithm}

%In this section, we discuss the resolution of~\eqref{eq:TV-BLASSO}.
Among all the methods that address the resolution of~\eqref{eq:TV-BLASSO}, a significant number of them are based on variations of the well-known Frank-Wolfe iterative algorithm, see, \textit{e.g.},~\cite{Bredies2013,Rao2015}.
In this paper, we particularize the \emph{sliding Frank-Wolfe} (SFW) algorithm proposed in~\cite{denoyelle2019}.
% to solve~\eqref{eq:TV-BLASSO}.


Starting from an initial guess (\textit{e.g.}, the null measure), SFW repeats the four following steps until convergence:
\begin{enumerate}
	\item Add a parameter (position of echo) to the support of the solution,
	\item Update all the coefficients solving a (finite dimensional) Lasso,
	\item Update jointly the position of the echoes  and the coefficients,
	\item Eventually remove parameters (echoes) associated to coefficients equal to zero.
\end{enumerate}
Finally, the algorithm stops as soon as an iterate satisfies the first order optimality condition associated to the convex problem~\eqref{eq:TV-BLASSO}.
Indeed, according to~\cite[Proposition 3.6]{Bredies2013} and denoting 
\begin{equation}
	\label{eq:def:mu_star}
	\mu^\star\triangleq\sum_{\ell=1}^k c_\ell^\star\spike{\theta_\ell^\star}
\end{equation}
the solution of~\eqref{eq:TV-BLASSO}, we have
\begin{equation}
	\label{eq:blasso-dual_certif}
	\sup_{\theta\in\Theta} \,\kinv{\lambda}\kvbar{ \kangle{
		\opObs{}\spike{\theta}, \bfy - \opObs{}\mu^\star
	}} 
	\leq 1
\end{equation}
as well as 
\begin{equation}
	\label{eq:blasso-dual_certif-sign}
	\kinv{\lambda} \kangle{\opObs{}\spike{\theta_\ell^\star}, \bfy - \opObs{}\mu^\star}
	=
	\mathrm{sign}(c_\ell^\star)
\end{equation}
for all $\ell\in\{1,\dotsc,k\}$.

The proposed SFW method for echo estimation is described by Algorithm~\ref{algo:FW}.
We now provide additional details about the implementation of each step.


\begin{algorithm}

	% \newcommand{\thetanew}{\theta_{\texttt{new}}}
	\newcommand{\thetanew}{\widehat{\theta}_t}

	\caption{
		\label{algo:FW}
		Sliding Frank-Wolfe algorithm for solving~\eqref{eq:TV-BLASSO}.
	}

	\KwIn{Observation operator $\opObs$, nonnegative scalar $\lambda$, precision $\varepsilon$}
	\KwOut{Channels represented as a measure $\widehat{\mu}$}
	%
	\BlankLine
	\tcp{Initialization}
	$\bfy \leftarrow \opObs{}\spike{(0,1)}$ \tcp{observation vector}
	$\widehat{\mu} = 0_{\calM}$ \tcp{estimated filters}
	%$\widehat{\calS} = \emptyset$ \tcp{estimated support}
	\BlankLine

	\tcp{Starting algorithm}
	\Repeat{until convergence}{
		$t \leftarrow 1$ \tcp{Iteration index}

		\tcp{1. Add new element to the support}
		Find $\thetanew\in \kargmax_{\theta\in\Theta} \kRe\kparen{\kangle{\opObs\spike{\theta}, \bfy - \opObs{}\widehat{\mu}}}$ \label{line:alg:FW:newTheta}\;
		$\widehat{c}_t \leftarrow 0$ \;
		%, see Algorithm~\ref{algo:FW-thetanew} \;
		\BlankLine

		$\eta \leftarrow \kinv{\lambda}\kRe\kparen{\kangle{\opObs\spike{\thetanew}, \bfy - \opObs{}\widehat{\mu}}}$ \;

		\If{ $\eta \leq 1+ \varepsilon$}{
			Stop --- $\widehat{\mu}$ is a solution \;
		}

		\tcp{2. Lasso update of the coefficients}
		$
		\displaystyle
		\widehat{c}_1,\dotsc,\widehat{c}_t \leftarrow \kargmin_{\bfc\in\kR+^t} \tfrac{1}{2} \big\Vert{\bfy - \sum_{\ell=1}^t \bfc_\ell \opObs\spike{\widehat{\theta}_\ell}}\big\Vert_2^2 + \lambda\kvvbar{\bfc}_1$ \label{line:alg:FW:lassoUpdate} \;


		\tcp{3. Joint update for a given number of spikes}
		$
		\displaystyle
		\widehat{\theta}_1\dotsc,\widehat{\theta}_t, \widehat{c}_1,\dotsc,\widehat{c}_t \leftarrow \kargmin_{\boldsymbol{\theta}\in\Theta^t,\bfc\in[0, x_{\max}]^t} \tfrac{1}{2} \big\Vert{\bfy - \sum_{\ell=1}^t \bfc_\ell \opObs\spike{\theta_\ell}}\big\Vert_2^2 + \lambda\kvvbar{\bfc}_1$ \label{line:alg:FW:jointUpdate} \;

		\tcp{4. Clean up}
		Set $\widehat{\mu} = \sum_{\ell=1}^t \widehat{c}_\ell \delta_{\widehat{\theta}_\ell}$ \;
		Eventually remove zero amplitudes Dirac masses from $\widehat{\mu}$ \;
		$t\leftarrow t+1$ \;
	}
\end{algorithm}






\vspace*{1em}

\noindent
\textbf{\itshape Nonnegative Blasso.}
To take into account the nonnegative constraint on the coefficients, the authors of~\cite{denoyelle2019} have proposed to slightly modify the SFW algorithm by \textit{i)} removing the absolute value in~\eqref{eq:blasso-dual_certif} and \textit{ii)} adding the non-negativity constraints at step 2 and 3 (see lines~\ref{line:alg:FW:lassoUpdate} and~\ref{line:alg:FW:jointUpdate} of Algorithm~\ref{algo:FW}).
The reader is referred to~\cite[remark~8 in Section~4.1]{denoyelle2019} for more details.

\vspace*{1em}

\noindent
\textbf{\itshape Real part in~\eqref{eq:blasso-dual_certif}.}
We have shown earlier that the solution $\mu^\star$ of \eqref{eq:TV-BLASSO} obeys~\eqref{eq:def:mu_star} and has to satisfy~\eqref{eq:blasso-dual_certif} and~\eqref{eq:blasso-dual_certif-sign}.
Here, due to the non-negativity constraint, condition~\eqref{eq:blasso-dual_certif-sign} rewrites for all $\ell\in\{1,\dotsc,k\}$
\begin{equation}
	\kinv{\lambda} \kangle{\opObs{}\spike{\theta_\ell^\star}, \bfy - \opObs{}\mu^\star}
	=
	\mathrm{sign}(c_\ell^\star) = 1
	.
\end{equation}
%where the last equality results from the non-negativity constraints.
Moreover, since the coefficients $\kfamily{c_\ell^\star}{\ell=1}^k$ are (nonnegative) scalars, one can show that~\eqref{eq:blasso-dual_certif} can be rewritten as (when removing the absolute value, see the previous paragraph)
\begin{equation}
	\sup_{\theta\in\Theta} \, \kinv{\lambda}\kRe\kparen{\kangle{
		\opObs{}\spike{\theta}, \bfy - \opObs{}\mu^\star
	}}
	\leq 1
	.
\end{equation}
In particular, using the real part in the implementation allows to remove the imaginary part that may appear due to the imprecisions.


\vspace*{1em}
\noindent
\textbf{\itshape Precision of the stopping criterion.}
We can only compute the solution~\eqref{eq:TV-BLASSO} up to some prescribed accuracy.
In particular, the condition~\eqref{eq:blasso-dual_certif} cannot be met due to the machine precision.
In this paper, we say that the algorithm stops as soon as
\begin{equation}
	\sup_{\theta\in\Theta} \, \kinv{\lambda}\kRe\kparen{\kangle{
		\opObs{}\spike{\theta}, \bfy - \opObs{}\mu^\star
	}} 
	\leq 1 + \varepsilon
	.
\end{equation}
Finally, we set $\varepsilon=10^{-3}$.



\vspace*{1em}
\noindent
\textbf{\itshape Finding new parameters (Line~\ref{line:alg:FW:newTheta}).}
The new parameter is found by solving
\begin{equation}
	\kargmax_{\theta\in\Theta} \kRe\kparen{\kangle{\opObs\spike{\theta}, \bfy - \opObs{}\widehat{\mu}}}
	.
\end{equation}
To solve this optimization problem, we first find a maximizer on a thin grid.\footnote{In particular, a grid of $20000$ points is considered in our experiments.}
We then proceed to a local refinement using the \texttt{scipy} optimization library\footnote{\label{note1} \url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html}}.
%\textcolor{red}{TODO: add size of the grid?}


\vspace*{1em}
\noindent
\textbf{\itshape Nonnegative Lasso (Line~\ref{line:alg:FW:lassoUpdate}).}
The nonnegative Lasso is solved using a custom implementation of a proximal gradient algorithm.
In particular, the procedure stops as soon as a stopping criterion in terms of duality gap is reached ($10^{-6}$).

\vspace*{1em}
\noindent
\textbf{\itshape Joint update (Line~\ref{line:alg:FW:jointUpdate}).}
In order to easier the numerical resolution and avoid thus avoid that some coefficients go to infinity, we show that the solution of
\begin{equation}
	\label{eq:joint-update}
	\min_{\boldsymbol{\theta}\in\Theta^t,\bfc\in\kR^t} \tfrac{1}{2} \big\Vert{\bfy - \sum_{\ell=1}^t \bfc_\ell \opObs\spike{\theta_\ell}}\big\Vert_2^2 + \lambda\kvvbar{\bfc}_1
\end{equation}
is equivalent to the solution of
\begin{equation}
	\kargmin_{\boldsymbol{\theta}\in\Theta^t,\bfc\in[0, x_{\max}]^t} \tfrac{1}{2} \big\Vert{\bfy - \sum_{\ell=1}^t \bfc_\ell \opObs\spike{\theta_\ell}}\big\Vert_2^2 + \lambda\kvvbar{\bfc}_1
\end{equation}
where 
\begin{equation}
	x_{\max} = \frac{1}{2\lambda} \kvvbar{\bfy}_2^2.
\end{equation}
Indeed, let us denote $\boldsymbol{\theta}^\star,\bfc^\star$ a minimizer of~\eqref{eq:joint-update}.
For any $\boldsymbol{\theta}\in\Theta^t$, the couple $\boldsymbol{\theta},{\bf0}_t$ is admissible for~\eqref{eq:joint-update} so we have by definition
\begin{equation}
	\tfrac{1}{2}\big\Vert{\bfy - \sum_{\ell=1}^t \bfc^\star_\ell \opObs\spike{\theta^\star_\ell}}\big\Vert_2^2 + \lambda\kvvbar{\bfc^\star}_1
	\leq
	\tfrac{1}{2} \big\Vert \bfy \big\Vert_2^2
	.
\end{equation}
Hence
\begin{equation}
	0 \leq c_\ell^\star \leq \kvvbar{\bfc^\star}_1 \leq \tfrac{1}{2\lambda} \big\Vert \bfy \big\Vert_2^2 \triangleq x_{\max}
		.
\end{equation}
Finally, the joint update of the coefficients and parameters is performed using the Sequential Least SQuares Programming (SLSQP) implemented in the \texttt{scipy} optimization library\cref{note1}.




