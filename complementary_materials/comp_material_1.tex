%!TEX root = ./comp_material.tex

\section{Reminder of the optimization problem}

\subsection{Signal and measurement model}

We a setup where a band-limited and square-integrable source signal $\contSource$ is emitted.
Due to the geometry of the room, the latter signal is both reflected (several times) and attenuated before reaching a set of two microphones.
The recorded signal at microphone $i\in\{1,2\}$ writes
\begin{equation}
    \label{eq:recordedSignal}
    \contRecordedSignal_i = \contSource \ast \contFilter_i^\star + \contNoise_i
\end{equation}
where $\ast$ denotes the (continuous) convolution operator, $\contNoise_i$ models some additive noise in the measurement process and $\contFilter_i^\star$ denotes the Acoustic Impulse Response (AIR).
Here, the notation $\star$ refers to the ground truth.

In AER, we are interested in AIR that are streams of Diracs, \textit{i.e.}, 
\begin{equation}
    \label{eq:def_filter_star}
    \contFilter_i^\star(t) = \sum_{k=1}^{K_i} c_{i,k} \delta(t - \tau_{i,k})
\end{equation}
where $K_i$ is the (unknown) number of echoes, $\kfamily{\tau_{i,k}}{k=1}^{K_i}$ models the echo delay, and $\kfamily{c_{i,k}}{k=1}^{K_i}$ are the corresponding non-negative attenuations.

In the noiseless case, that is when $\contNoise_i=0$ for $i\in\{1,2\}$, we have the identity
\begin{equation} \label{eq:cross-relation}
    \contRecordedSignal_1 \ast \contFilter_2^\star = \contRecordedSignal_2 \ast \contFilter_1^\star
\end{equation}
by associativity of the convolution operator.
This result is dubbed \emph{cross-relation} identity in the channel identification literature \cite{Xu1995}.
Hence, one can expect to recover the two filters by solving an optimization problem involving~\eqref{eq:cross-relation}.
%the difference between the two terms in~\eqref{eq:cross-relation}.

However, in practice, only sampled versions of the two recorded signals are available.
More precisely, we consider a  measurement model where the incoming signal undergoes an ideal low-pass filter $\idealLowPassFilter$ with frequency support $\kintervcc{-\sfrac{\SamplingFreq}{2}}{\sfrac{\SamplingFreq}{2}}$ before being regularly sampled at the rate  $\SamplingFreq$. 
We denote $\disRecordedSignal_1,\disRecordedSignal_2\in\kR^N$ the two vectors of $N$ (consecutive) samples defined $\forall n \in\{1, \dots, N\}$ and $i\in\{1, 2\}$ by %\remCE{Todo: $\idealLowPassFilter$}
\begin{equation}
    \label{eq:measurement-process}
    \disRecordedSignal_i[n] =
    \kparen{\idealLowPassFilter \ast \contRecordedSignal}\kparen{\frac{n}{\SamplingFreq}}
    .
\end{equation}





%!TEX root = ../icassp2020braire.tex

%\textcolor{red}{Todo:
%\begin{itemize}
    %\item Find the correct number of Fourier coefficient
    %
    %\item Renormalized atom?
    %\item Define what is a filter - function in the first section and measure in the second?
    %Or directly talk about radon measure since the beginning?
    %
    %\item Introduce the CTDF of $\contFilter$ in the equation (instead of the Fourier transform)
    %
    %\item Introduce the dictionary before~\eqref{eq:TV-BP}.
%\end{itemize}
%}

%\textcolor{blue}{What do we do with the door in time domain? in Mulan they say that $X_i$ is a good approximation of the FT as $N$ tends to infinity right?
%$\rightarrow$ sinus cardinal, has to be cancelled? could be done since the the source signal is assumed band limited with a correct sampling frequency}




%\remCE{TODO intro after related work is written}
%All the methods presented above rely on a discrete formulation of the Cross-Relation identity~\eqref{eq:cross-relation}, thus relying on a T\oe{}plitz formulation of the convolution.
%\remCE{Add drawbacks, such as on-the-grid}
%In this work, we propose to circumvent this issue by casting the estimation problem in the \emph{continuous dictionaries} framework.


\subsection{Cross-relation in the Fourier domain}

We first remark that the cross-relation identity~\eqref{eq:cross-relation} ensures that
\begin{equation}
    \idealLowPassFilter 
    \ast \contRecordedSignal_1 
    \ast  \contFilter_2^\star
    =
    \idealLowPassFilter 
    \ast \contRecordedSignal_2
    \ast  \contFilter_1^\star
\end{equation}
by associativity of the convolution operator, or equivalently
\begin{equation}
    \label{eq:cross-relation-identity-fourier}
    \fourierTrans(\idealLowPassFilter\ast\contRecordedSignal_1) \cdot \fourierTrans \contFilter_2^\star
    =
    \fourierTrans(\idealLowPassFilter\ast\contRecordedSignal_2) \cdot \fourierTrans \contFilter_1^\star
\end{equation}
where $\fourierTrans$ denotes the Fourier transform (FT) and is such that\footnote{We use the same notation when referring to the Fourier transform of a function and a distribution.}
\begin{equation}
    \kforall[f\in\kR]\quad \fourierTrans y(f) = 
    \int_{-\infty}^{+\infty} y(t)\cste^{-\csti2\pi f t}\,\mathrm{d}t
\end{equation}
for any signal or filter $y$.
However, due to the measurement process described in~\eqref{eq:measurement-process}, one has only access to the lower end of the spectrum of $\contRecordedSignal_i$.
More precisely, we have access to the lowest $N$ coefficients\footnote{To be more precise, $2N-1$ coefficients are available. However, by symmetry of the DFT for real signals, $N$ coefficients are sufficient.}
of the Fourier series given by
\begin{equation}
    \label{eq:dft-Xi}
    \RecordedSignalDFT_i[f] = \sum_{n=0}^{N-1}
    \contRecordedSignal_i(\tfrac{n}{\SamplingFreq})
    \cste^{-\csti2\pi fn/\SamplingFreq}
\end{equation}
where $X_i$ denotes the Discrete Fourier transform of $\contRecordedSignal_i$ and $f$ belongs to the \emph{finite} set of \emph{regularly-spaced} frequencies $\kfamily{n\SamplingFreq}{n=0}^{N-1}$.
Note that~\eqref{eq:dft-Xi} is only an approximation of the Fourier series of $\idealLowPassFilter\ast\contFilter_i$ which becomes nevertheless increasingly accurate as $N$ tends to infinity.
    



\subsection{Echo localization with continuous dictionaries}


Although the whole spectrum of $\idealLowPassFilter\ast\contRecordedSignal_i$ is not available, the identity~\eqref{eq:cross-relation-identity-fourier} remains true for the first $N$ Fourier coefficients.
Moreover, the FT of any filter $\contFilter$ satisfying~\eqref{eq:def_filter_star} is known in \emph{closed-form} and can be interpreted as \emph{a parametric atom}.
Hence we propose to cast the problem of AIR estimation into the framework of \emph{Continuous dictionaries}.
To that aim, let us define the so-called \emph{parameter set}
\begin{equation}
    \label{eq:parameter-set}
    \Theta \triangleq \kintervcc{0}{T} \times \kbrace{1, 2}
\end{equation}
where $T$ is the length (in time) of the filter.
We let the reader check that $\Theta$ is a compact metrizable set.
Then, the two desired filters  $\contFilter_1^\star,\contFilter_2^\star$ given  by~\eqref{eq:def_filter_star} can be uniquely\footnote{Uniqueness is ensured as soon as we impose $c_{j,k}>0$ $\forall j,k$.} represented by the following discrete measure over $\Theta$
\begin{equation}
    \label{eq:representation_filter_measure}
    \mu^\star = \sum_{j=1}^{2} \sum_{k=1}^{K_j} c_{j,k} \delta_{(\tau_{j,k}, j)}.
\end{equation}
The rational behind~\eqref{eq:parameter-set}  and~\eqref{eq:representation_filter_measure} is as follows.
A couple of filters is now represented by a single stream of Diracs, where we have considered an augmented variable $j$ indicating to which filter the spike belongs.
For instance, a Dirac in $(\tau, j)$ indicates that the $j$-th filter contains a Dirac at $\tau$.

The set of all measures over $\Theta$ (\textit{i.e.}, the set of all couples of  filters) is equipped with the Total-Variation norm (TV-norm) defined for any measure $\mu$ as
\begin{equation}
    \normTV{\mu} \triangleq
    \sup_{P}\; \sum_{E\in P} \kvbar{
        \mu(E)
    }
\end{equation}
where the supremum is taken over all partitions $P$ of $\Theta$ into a finite number of disjoint measurable subsets.\footnote{See~\cite{Rudin1987} for a rigorous construction of measures set and the TV-norm.}
In this work, we restrict our attention to the set $\posDisRadonMeasure$ of unsigned and discrete Radon measures\footnote{\textit{i.e.}, linear combinations of Dirac with positive coefficients.} over $\Theta$ with \emph{finite} TV-norm.
We now define the \emph{linear} observation operator $\kfuncdef{\opObs}{\posDisRadonMeasure}{\kC^{N}}$, which is such that
\begin{equation}
    \opObs\delta_{(\tau, j)}
    =
    \begin{cases}
        - X_1 \odot {\calF_N \delta_{\tau}}  &\text{ if } j=1 \\
        + X_2 \odot {\calF_N\delta_{\tau}}  &\text{ if } j=2.
    \end{cases}
\end{equation}
$\forall(\tau,j)\in\Theta$ where the two complex vectors $X_1,X_2$ have been defined in~\eqref{eq:dft-Xi}, $\odot$ denotes the component-wise Hadamard product and $\calF_N$ refers to the vector of complex exponential
\begin{equation}
    \calF_N\delta_{\tau} =
    \ktranspose{
    \begin{pmatrix}
        \cste^{-\csti2\pi\tfrac{0}{T_e}\tau} &
        \hdots &
        \cste^{-\csti2\pi\tfrac{N-1}{T_e}\tau}
    \end{pmatrix}
    }
    \in\kC^N
    .
\end{equation}
%and the minimization is carried over the space of finite Radon measures.
Then, by linearity of the observation operator $\opObs$, the relation~\eqref{eq:cross-relation-identity-fourier} evaluated at the $N$ considered frequencies rewrites
\begin{equation}
    \label{eq:cross-relation-measure}
    \opObs\mu^\star = {\bf0}_{N}
    .
\end{equation}
Before continuing our exposition, we note that the anchor constraint can be written in a more convenient way.
Indeed, the constraint $\mu(\{(0, 1)\})=1$ ensures the existence of a Dirac at $0$ in the filter 1.
Then, the targeted filter rewrites
\begin{equation}
    \mu^\star = \delta_{(0, 1)} + \widetilde{\mu}^\star
\end{equation}
where $\widetilde{\mu}^\star$ is a (finite) discrete measure verifying  
\begin{equation}
	\widetilde{\mu}^\star\kparen{\{(0, 1)\}} = 0.
\end{equation}
Denoting $\bfy\triangleq\opObs\delta_{(0, 1)}\in\kC^{N}$, the relation~\eqref{eq:cross-relation-measure} becomes
\begin{equation}
    \label{eq:cross-relation-measure-and-obs}
    \opObs\widetilde{\mu}^\star = \bfy
    .
\end{equation}
For the sake of clarity, we use these conventions hereafter and omit the tilde.
Now, following~\cite{Castro2012aa,Carlos2014}, one can expect to recover the desired filter $\mu^\star$ by solving
\begin{equation}
    \stepcounter{equation}
    \tag{\theequation-$\calP^0_{\text{\texttt{TV}}}$}
    \label{eq:TV-BP}
    %\begin{split}
    \widehat{\mu}
    = 
    \kargmin_{\posDisRadonMeasure}
    \;
    \normTV{
        \mu
    }
    %\\
    %
    \quad
    \text{s.t.}
    \quad
    \begin{cases}
        \opObs\mu
        = \bfy \\
        \mu(\{(0, 1)\}) = 0.
    \end{cases}
    %\end{split}
\end{equation}
Note that~\eqref{eq:TV-BP} has to be interpreted as a natural extension of the well-known \emph{Basis-Pursuit} problem to the continuous setting.
Indeed, for \emph{any} finite discrete measure $\mu = \sum_{k=1}^K c_k\delta_{(\tau_k, j_k)}$, the TV-norm of $\mu$ returns to the $\ell_1$-norm of the coefficients, \textit{i.e.}, 
\begin{equation}
	\kvvbar{\mu}_{TV} = \sum_{k=1}^K \kvbar{c_k}.
\end{equation}
In case of noise during the measurement process (\textit{i.e.},  $n_i\neq0$ in~\eqref{eq:recordedSignal}), the first equality constraint in~\eqref{eq:TV-BP} can be relaxed, leading to the so-called Beurling-LASSO (BLASSO) problem
\begin{equation}
    \stepcounter{equation}
    \tag{\theequation-$\calP^\lambda_{\text{\texttt{TV}}}$}
    \label{eq:TV-BLASSO}
    \begin{split}
    \widehat{\mu}
    = 
    \kargmin_{\mu \in\posDisRadonMeasure}
    \;
    \tfrac{1}{2} \kvvbar{
        \bfy - \opObs\mu
    }_2^2
    +
    \lambda\normTV{
        \mu
    }
    \\
    %
    \quad
    \text{s.t.}
    \quad
    \mu(\{(0, 1)\}) = 0
    .
    \end{split}
\end{equation}
We emphasize that although continuous Radon measures may potentially be admissible, the minimizers of~\eqref{eq:TV-BLASSO} are \emph{guarantied} to be streams of Dirac\textit{s}~\cite[Theorem~4.2]{bredies2018sparsity}.
%In addition, although problem~\eqref{eq:TV-BLASSO} seems to depend on some regularization parameter $\lambda$, we describe in Section~\ref{sec:xp} a procedure to automatically tune it to recover the desired number of spikes.

Finally, note that problem~\eqref{eq:TV-BLASSO} is convex with linear constraints.
Hence, it can be solved with standard convex optimization methods.
We discuss the resolution of~\eqref{eq:TV-BLASSO} in the next section.

% In this work, we particularize the \emph{sliding Frank-Wolfe} algorithm proposed in~\cite{denoyelle2019} to solve~\eqref{eq:TV-BLASSO}. Detailed descriptions of the steps of the algorithm are given in the supplementary material\footnote{\url{https://gitlab.inria.fr/panama-team/blaster/blob/master/algorithm.pdf}.}.



